import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BlockUIModule } from 'ng-block-ui';
import { JwtModule, JwtModuleOptions } from "@auth0/angular-jwt";
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { UserRegisterComponent } from './components/modals/user-register/user-register.component';
import { MatCardModule } from '@angular/material/card';
import { MatFormFieldModule } from '@angular/material/form-field';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { MatDialogModule } from '@angular/material/dialog';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { LoginService } from './service/login.service';
import { HomeComponent } from './components/home/home.component';
import { MatIconModule } from '@angular/material/icon';
import { HttpInterceptorImpl } from './utils/http.interceptor';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MyPostsComponent } from './components/modals/my-posts/my-posts.component';
import { MatTabsModule } from '@angular/material/tabs';
import { CreatePostComponent } from './components/modals/create-post/create-post.component';
import { TextFieldModule } from '@angular/cdk/text-field';
import { MatDividerModule } from '@angular/material/divider';
import { CreateAlbumComponent } from './components/modals/create-album/create-album.component';
import { MatSelectModule } from '@angular/material/select';
import { CreatePhotoComponent } from './components/modals/create-photo/create-photo.component';
import { ToastrModule } from 'ngx-toastr';
import { CommonModule } from '@angular/common';

const jwtOpt: JwtModuleOptions = { config: { tokenGetter: getToken } };
export function getToken(): any {
  return localStorage.getItem('token');
}

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    UserRegisterComponent,
    HomeComponent,
    MyPostsComponent,
    CreatePostComponent,
    CreateAlbumComponent,
    CreatePhotoComponent
  ],
  imports: [
    CommonModule,
    BrowserModule,
    AppRoutingModule,
    BlockUIModule.forRoot({
      message: 'Carregando...'
    }),
    BrowserAnimationsModule,
    MatCardModule,
    MatFormFieldModule,
    FormsModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatInputModule,
    MatDialogModule,
    MatSlideToggleModule,
    HttpClientModule,
    JwtModule.forRoot(jwtOpt),
    MatIconModule,
    MatGridListModule,
    MatTooltipModule,
    MatTabsModule,
    TextFieldModule,
    MatDividerModule,
    MatSelectModule,
    ToastrModule.forRoot()
  ],
  providers: [
    LoginService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpInterceptorImpl,
      multi: true,
    },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
