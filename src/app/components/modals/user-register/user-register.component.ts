import { Component, Inject, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ToastrService } from 'ngx-toastr';
import { User } from 'src/app/model/user.model';

@Component({
  selector: 'app-user-register',
  templateUrl: './user-register.component.html',
  styleUrls: ['./user-register.component.css']
})
export class UserRegisterComponent implements OnInit {

  user = new User();
  email = new FormControl('', [Validators.required, Validators.email]);
  pass = new FormControl('', [Validators.required, Validators.minLength(4)]);

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private dialogRef: MatDialogRef<UserRegisterComponent>,
    private msgService: ToastrService,
  ) { }

  ngOnInit(): void {
    this.checkEmail()
  }

  checkEmail() {
    this.user.email = this.data.email;
  }

  close() {
    this.dialogRef.close();
  }

  register() {
    
    if(this.checkFields()){
      this.user.email = this.email.value;
      this.user.password = this.pass.value;
      this.dialogRef.close(this.user)
    }
  }

  checkCpf() {
    let strCPF = this.user.cpf
    if(strCPF.length >= 11){
      var Soma;
      var Resto;
      Soma = 0;
      if (strCPF == "00000000000") return false;

      for (let i = 1; i <= 9; i++) Soma = Soma + parseInt(strCPF.substring(i - 1, i)) * (11 - i);
      Resto = (Soma * 10) % 11;

      if ((Resto == 10) || (Resto == 11)) Resto = 0;
      if (Resto != parseInt(strCPF.substring(9, 10))) return false;

      Soma = 0;
      for (let i = 1; i <= 10; i++) Soma = Soma + parseInt(strCPF.substring(i - 1, i)) * (12 - i);
      Resto = (Soma * 10) % 11;

      if ((Resto == 10) || (Resto == 11)) Resto = 0;
      if (Resto != parseInt(strCPF.substring(10, 11))) return false;
      return true;

    }else{
      return false;
    }
  }


  checkFields(){
    if(this.user.name == null || this.pass.value == null || this.email.value == null || this.user.cpf == null){
      this.msgService.warning('Todos os campos são obrigatórios!')
      return false;
    }

    if(!this.checkCpf()){
      this.msgService.warning('CPF inválido!')
      return false;
    }

    if(!this.email.valid){
      this.msgService.warning('Email inválido!')
      return false;
    }

    return true;
  }
}
