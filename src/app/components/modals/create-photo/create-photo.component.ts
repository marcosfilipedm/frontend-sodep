import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { LoginService } from 'src/app/service/login.service';

@Component({
  selector: 'app-create-photo',
  templateUrl: './create-photo.component.html',
  styleUrls: ['./create-photo.component.css']
})
export class CreatePhotoComponent implements OnInit {

  albumSelected: any = null;
  albuns:any = [];
  file:any;
  url:any
  user: any;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private dialogRef: MatDialogRef<CreatePhotoComponent>,
    private loginService: LoginService,
  ) { }

  ngOnInit(): void {
    this.user = this.loginService.getUserLog();
    this.albuns = this.filterAlbum(this.data.albuns);
    this.filterAlbum;
  }

  filterAlbum(albuns:any[]){
    return albuns.filter(x => x.user.id == this.user.id)
  }

  incomingfile(event: any) {
    if (event.target.files && event.target.files[0]) {
      this.file = event.target.files[0];
      var reader = new FileReader();

      reader.readAsDataURL(event.target.files[0]); // read file as data url

      reader.onload = () => { // called once readAsDataURL is completed
        this.url = reader.result;
      }
    }
  }

  upload() {
    if(this.file != null) {
      let obj = {file: this.file, album: this.albumSelected}
      this.dialogRef.close(obj);
    } else {
      //this.msgService.msgAdvertencia("Escolha um arquivo");
    }
  }

  close(){
    this.dialogRef.close()
  }
}
