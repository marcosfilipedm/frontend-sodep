import { useAnimation } from '@angular/animations';
import { Component, Inject, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ToastrService } from 'ngx-toastr';
import { AlbumService } from 'src/app/service/album.service';
import { LoginService } from 'src/app/service/login.service';
import { PhotoService } from 'src/app/service/photo.service';
import { PostService } from 'src/app/service/post.service';
import { CreateAlbumComponent } from '../create-album/create-album.component';
import { CreatePhotoComponent } from '../create-photo/create-photo.component';

@Component({
  selector: 'app-my-posts',
  templateUrl: './my-posts.component.html',
  styleUrls: ['./my-posts.component.css']
})
export class MyPostsComponent implements OnInit {

  postList:any = [];
  alterVisibility: boolean = false;
  albuns: any = [];
  photos: any = [];
  user: any;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private postService: PostService,
    private dialog: MatDialog,
    private albumService: AlbumService,
    private photoService: PhotoService,
    private loginService: LoginService,
    private msgService: ToastrService,
  ) { }

  ngOnInit(): void {
    this.user = this.loginService.getUserLog();
    this.postList = this.data.postList
    this.getAlbuns();
  }

  alterStatus(idPost :any){
    this.postService.alterStatus(idPost).subscribe(
      res => {
        if(res)
          this.updateList()
      }
    )
  }

  updateList(){
    this.postService.getMyPosts().subscribe(
      res => this.postList = res
    )
  }

  openCreateAlbum(){
    let dialogRef = this.dialog.open(CreateAlbumComponent, {
      width: '300px',
      data: {
        albuns: null
      }
    });
    dialogRef.afterClosed().subscribe(res => {
      if(this.checkResponse(res)){
        this.createAlbum(res)
      }
    })
  }

  createAlbum(albumName:any){
    this.albumService.save(albumName).subscribe(
      res => {
        if(this.checkResponse(res))
          this.msgService.success('Salvo com sucesso.')
          this.getAlbuns()
      }
    )
  }

  getAlbuns(){
    this.albumService.getAlbuns().subscribe(
      res => this.albuns = res
    )
  }

  addAlbum(postId:any){
    let dialogRef = this.dialog.open(CreateAlbumComponent, {
      width: '300px',
      data: {
        albuns: this.albuns
      }
    });
    dialogRef.afterClosed().subscribe(res => {
      if(this.checkResponse(res)){
        this.postService.addAlbum(postId,res).subscribe(
          res => {
            if(res != null)
              this.msgService.success('Salvo com sucesso.')
          }
        )
      }
    })
  }

  getPhotos(idAlbum:any){
    this.photoService.getPhoto(idAlbum).subscribe(
      res => {
        this.photos = res
      }
    )
  }

  savePhoto(){
    let dialogRef = this.dialog.open(CreatePhotoComponent, {
      width: '30%', height: 'auto',
      data: { 
        albuns: this.albuns
      }
    });
    dialogRef.afterClosed().subscribe(res => {
      if(this.checkResponse(res)){
        this.photoService.savePhoto(res.album, res.file).subscribe(
          res => {
            this.msgService.success('Salvo com sucesso.')
          }
        )
      }
    })
  }

  checkResponse(res:any){
    if(res == null || res == undefined || res == ''){
      return false;
    }else{
      return true;
    }
  }
}
