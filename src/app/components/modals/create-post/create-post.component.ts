import { Component, Inject, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { PostService } from 'src/app/service/post.service';
import { CreateAlbumComponent } from '../create-album/create-album.component';

@Component({
  selector: 'app-create-post',
  templateUrl: './create-post.component.html',
  styleUrls: ['./create-post.component.css']
})
export class CreatePostComponent implements OnInit {

  legend: string = '';
  file:any;
  url:any;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private dialogRef: MatDialogRef<CreatePostComponent>,
    private postService: PostService,
  ) { }

  ngOnInit(): void {

  }

  incomingfile(event: any) {
    if (event.target.files && event.target.files[0]) {
      this.file = event.target.files[0];
      var reader = new FileReader();

      reader.readAsDataURL(event.target.files[0]); // read file as data url

      reader.onload = () => { // called once readAsDataURL is completed
        this.url = reader.result;
      }
    }
  }

  upload() {
    if(this.file != null) {
      let response = {file: this.file, legend: this.legend};
      this.dialogRef.close(response);
    } else {
      //this.msgService.msgAdvertencia("Escolha um arquivo");
    }
  }

}
