import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AlbumService } from 'src/app/service/album.service';
import { LoginService } from 'src/app/service/login.service';

@Component({
  selector: 'app-create-album',
  templateUrl: './create-album.component.html',
  styleUrls: ['./create-album.component.css']
})
export class CreateAlbumComponent implements OnInit {

  name: string = '';
  albumSelected: any = null;
  albuns:any = [];
  user: any;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private dialogRef: MatDialogRef<CreateAlbumComponent>,
    private loginService: LoginService,
  ) { }

  ngOnInit(): void {
    this.user = this.loginService.getUserLog();
    this.albuns = this.filterAlbum(this.data.albuns);
  }

  filterAlbum(albuns:any[]){
    return albuns.filter(x => x.user.id == this.user.id)
  }

  save(){
    if(this.data.albuns == null)
      this.dialogRef.close(this.name)
    else
      this.dialogRef.close(this.albumSelected)
  }

  close(){
    this.dialogRef.close();
  }
}
