import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Comment } from 'src/app/model/comment.model';
import { User } from 'src/app/model/user.model';
import { CommentService } from 'src/app/service/comment.service';
import { LoginService } from 'src/app/service/login.service';
import { PostService } from 'src/app/service/post.service';
import { CreatePostComponent } from '../modals/create-post/create-post.component';
import { MyPostsComponent } from '../modals/my-posts/my-posts.component';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})

export class HomeComponent implements OnInit {

  constructor(
    private postService: PostService,
    private loginServide: LoginService,
    private dialog: MatDialog,
    private commentService: CommentService,
    private msgService: ToastrService,
  ) { }

  postArray:any = [];
  user:any = new User;
  
  ngOnInit(): void {
    this.getUser();
    this.getPosts();
  }

  getUser(){
    this.loginServide.getUser().subscribe(
      res => this.user = res
    )
  }

  getPosts(){
    this.postService.getPosts().subscribe(
      res => {
        this.postArray = res;
      }
    )
  }

  getMyPosts(){
    this.postService.getMyPosts().subscribe(
      res => {
        this.openPostsModal(res)
      }
    )
  }

  openPostsModal(postList: any){
    let dialogRef = this.dialog.open(MyPostsComponent, {
      width: '50%',
      height: '80%',
      data: { 
        postList: postList
      }
    });
    dialogRef.afterClosed().subscribe(res => {
      this.getPosts();
    })
  }

  createPostsModal(){
    let dialogRef = this.dialog.open(CreatePostComponent, {
      width: '30%', height: 'auto'
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result != null && result != undefined) {
        let posicaoPontoExtensao = result.file.name.lastIndexOf(".");
        let extensao = result.file.name.substring(posicaoPontoExtensao + 1, result.file.name.length).toLocaleLowerCase();
        this.postService.createPost(result.legend, extensao, result.file).subscribe(
          res => {
            if(res != null){
              this.msgService.success('Salvo com sucesso')
              this.getPosts();
            }
          }
        )
      }
    })
  }

  saveComment(obj: any){
    let comment: Comment = new Comment()
    let post = {id: obj.id}
    let user = {id: obj.user.id}
    comment.post = post
    comment.comment = obj.comment;
    comment.user = user

    if(this.checkComment(obj.comment)){
      this.msgService.warning('Comentário inválido');
      return 
    }
      
    
    this.commentService.saveComment(comment).subscribe(
      res => {
        this.msgService.success('Salvo com sucesso')
        this.getPosts();
      }
    )
  }

  checkComment(comment: string){
    if(comment == null || comment == '' || comment == undefined)
      return true
    else
      return false
  }
}
