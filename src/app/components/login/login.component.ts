import { error } from '@angular/compiler/src/util';
import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { LoginService } from 'src/app/service/login.service';
import { UserService } from 'src/app/service/user.service';
import { UserRegisterComponent } from '../modals/user-register/user-register.component';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  email = new FormControl('', [Validators.required]);
  pass = new FormControl('', [Validators.required, Validators.minLength(4)]);

  constructor(
    private dialog: MatDialog,
    private loginService: LoginService,
    private router: Router,
    private userService: UserService,
    private msgService: ToastrService,
  ) { }

  ngOnInit(): void {
    this.loginService.logout();
  }

  singIn(){
    this.checkCredencials();

    this.loginService.singIn(this.email.value, this.pass.value).subscribe(
      res => {
        this.loginService.saveToken(res);
        this.loginService.getUser().subscribe(
          res => {
            this.loginService.saveUser(JSON.stringify(res));
            this.router.navigate(['home']);
          }
        )
      },error => {
        this.msgService.error('Credênciais inválidas')
      }
    );
  }

  checkCredencials(){
    if(!this.email.valid || !this.pass.valid){
      this.msgService.warning('Credenciais inválidas')
      return;
    }
  }

  userRegister() {
    let dialogRef = this.dialog.open(UserRegisterComponent, {
      width: '600px',
      data: { 
        email: this.email.value,
      }
    });
    dialogRef.afterClosed().subscribe(res => {
      if(res!=null){
        this.checkCredencials();
        this.userService.createUser(res).subscribe(
          res => {
            if(res != null)
              this.msgService.success('Salvo com sucesso.')
            else
              this.msgService.error('Erro ao salvar usuário.')
          }
        )
      }
    })
  }
}
