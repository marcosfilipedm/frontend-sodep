export class User {
    id: any = null;
    name: string = '';
    email!: string;
    cpf!: string;
    password!: string;
    status!: boolean;
}