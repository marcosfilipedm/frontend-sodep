import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse } from "@angular/common/http";
import { Router } from "@angular/router";
import { BlockUI, NgBlockUI } from "ng-block-ui";
import { Observable, throwError } from "rxjs";
import { LoginService } from "../service/login.service";
import { catchError, tap } from 'rxjs/operators';
import { Injectable } from "@angular/core";
import { ToastrService } from "ngx-toastr";

@Injectable()
export class HttpInterceptorImpl implements HttpInterceptor {

    @BlockUI() blockUI!: NgBlockUI;

    constructor(
        private router: Router, 
        private loginService: LoginService,
        private msgService: ToastrService,
        ) { }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

         let authorization = this.loginService.getToken() == null ? '' : this.loginService.getToken();

        /*
        verifica se a requisicao ja possui um content-type definido
        se ja possuir, eh ele que sera utilizado na requisicao,
        se nao possuir, o default sera application/json
        */
        /*
        let contentType: string;

        let content = req.headers.getAll('Content-Type');
        if (content != null) {
            contentType = content[0];
        } else {
            contentType = 'application/json';
        }
        */
        const newRequest = req.clone({
            setHeaders: {
                Authorization: authorization
            }
        });

        this.blockUI.start();

        return next.handle(newRequest).pipe(tap((ev: HttpEvent<any>) => {
            if (ev instanceof HttpResponse) {
                this.blockUI.stop()
            }
        }), 
        catchError(
            response => {
                if (response instanceof HttpErrorResponse) {
                    if (response.status === 401 || response.status === 403) {
                        this.loginService.logout();
                        this.msgService.error('Token inspirado!')
                    }
                    if (response.status === 500) {
                        this.msgService.error('Ocorreu um erro no servidor')
                    }

                    if (response.status === 0) {
                        this.msgService.error('Ocorreu um erro no servidor')
                    }
                }
                this.blockUI.stop();
                return throwError(response);
            }
        ),
        )
    }

}

