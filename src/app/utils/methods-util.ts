export class MethodsUtils{

    public static validarCPF(cpf: any) {
        let strCPF = cpf.toString();
        let soma;
        let resto;
        soma = 0;
        if (strCPF == "00000000000") return { validarCPF: { valid: false } };

        for (let i = 1; i <= 9; i++) soma = soma + parseInt(strCPF.substring(i - 1, i)) * (11 - i);
        resto = (soma * 10) % 11;

        if ((resto == 10) || (resto == 11)) resto = 0;
        if (resto != parseInt(strCPF.substring(9, 10))) return { validarCPF: { valid: false } };

        soma = 0;
        for (let i = 1; i <= 10; i++) soma = soma + parseInt(strCPF.substring(i - 1, i)) * (12 - i);
        resto = (soma * 10) % 11;
        if ((resto == 10) || (resto == 11)) resto = 0;
        if (resto != parseInt(strCPF.substring(10, 11))) return { validarCPF: { valid: false } };
        return null;
    }

    public static validarCNPJ(cnpj: any) {
        let strCNPJ = cnpj;

        if (strCNPJ == '') {
            return { validarCNPJ: { valid: false } };
        }

        if (strCNPJ.length != 14) {
            return { validarCNPJ: { valid: false } };
        }

        // Elimina CNPJs invalidos conhecidos
        if (strCNPJ == "00000000000000" ||
            strCNPJ == "11111111111111" ||
            strCNPJ == "22222222222222" ||
            strCNPJ == "33333333333333" ||
            strCNPJ == "44444444444444" ||
            strCNPJ == "55555555555555" ||
            strCNPJ == "66666666666666" ||
            strCNPJ == "77777777777777" ||
            strCNPJ == "88888888888888" ||
            strCNPJ == "99999999999999") {
            return { validarCNPJ: { valid: false } };
        }

        // Valida DVs
        let tamanho = strCNPJ.length - 2
        let numeros = strCNPJ.substring(0, tamanho);
        let digitos = strCNPJ.substring(tamanho);
        let soma = 0;
        let pos = tamanho - 7;
        for (let i = tamanho; i >= 1; i--) {
            soma += numeros.charAt(tamanho - i) * pos--;
            if (pos < 2) {
                pos = 9;
            }
        }
        let resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
        if (resultado != digitos.charAt(0)) {
            return { validarCNPJ: { valid: false } };
        }
        tamanho = tamanho + 1;
        numeros = strCNPJ.substring(0, tamanho);
        soma = 0;
        pos = tamanho - 7;
        for (let i = tamanho; i >= 1; i--) {
            soma += numeros.charAt(tamanho - i) * pos--;
            if (pos < 2) {
                pos = 9;
            }
        }
        resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
        if (resultado != digitos.charAt(1)) {
            return { validarCNPJ: { valid: false } };
        }
        return null;
    }
}