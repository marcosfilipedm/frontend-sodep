import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AlbumService {

  public readonly API_URL = environment.urlBase;
  
  constructor(
    private http: HttpClient,
  ) { }

  save(albumName: string): Observable<any>{
    return this.http.post(this.API_URL + '/album', albumName)
  }

  getAlbuns(): Observable<any>{
    return this.http.get(this.API_URL + '/album')
  }

  getPhotos(idAlbum:any): Observable<any>{
    return this.http.get(this.API_URL + '/album/' + idAlbum + '/photos')
  }
}
