import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CommentService {

  public readonly API_URL = environment.urlBase;
  
  constructor(
    private http: HttpClient
  ) { }

  saveComment(comment: any): Observable<any>{
    return this.http.post(this.API_URL + "/comment/save", JSON.stringify(comment), {
      headers: {'Content-Type':'application/json; charset=utf-8'}
    });
  }
}
