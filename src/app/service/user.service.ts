import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { User } from '../model/user.model';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  public readonly API_URL = environment.urlBase;
  
  constructor(
    private http: HttpClient
  ) { }

  createUser(user:any): Observable<User>{
    return this.http.post<User>(this.API_URL + "/user/public/save", JSON.stringify(user), {
      headers: {'Content-Type':'application/json; charset=utf-8'}
    });
  }
}
