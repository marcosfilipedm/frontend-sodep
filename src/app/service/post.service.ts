import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PostService {

  public readonly API_URL = environment.urlBase;
  
  constructor(
    private http: HttpClient
  ) { }

  createPost(legend: any, ext: any, file: any): Observable<any>{
    let data = new FormData()
    data.append('imageFile', file)
    data.append('legend', legend)
    data.append('ext', ext)
    return this.http.post(this.API_URL + "/post/save", data)
  }

  getPosts(): Observable<any>{
    return this.http.get(this.API_URL + "/post")
  }

  getMyPosts(): Observable<any>{
    return this.http.get(this.API_URL + "/post/my-posts")
  }

  alterStatus(idPost: any): Observable<any>{
    return this.http.put(this.API_URL + "/post/alter-status", idPost)
  }

  addAlbum(post: any, album: any): Observable<any>{
    let data = new FormData()
    data.append('post', post)
    data.append('album', album)
    return this.http.put(this.API_URL + "/post/add-album", data)
  }
}
