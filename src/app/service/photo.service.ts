import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PhotoService {
  
  public readonly API_URL = environment.urlBase;
  
  constructor(
    private http: HttpClient
  ) { }

  savePhoto(album: any, file: any): Observable<any>{
    let data = new FormData()
    data.append('imageFile', file)
    data.append('album', album)
    return this.http.post(this.API_URL + "/photo", data)
  }

  getPhoto(album:any): Observable<any>{
    return this.http.get(this.API_URL + "/photo/" + album)
  }
}
