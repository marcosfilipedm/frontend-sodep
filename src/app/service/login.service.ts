import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  public readonly API_URL = environment.urlBase;
  
  constructor(
    private http: HttpClient,
    private router: Router,
  ) { }

  singIn(user: string, pass: string): Observable<any>{
    let headers: HttpHeaders = new HttpHeaders();
    headers = headers.append('username', user);
    headers = headers.append('password', pass);
    return this.http.get(this.API_URL + "/user/public/login", {headers: headers});
  }

  logout(): void {
    localStorage.removeItem('token');
    this.router.navigate(['login']);
  }

  getToken(): any {
    return localStorage.getItem('token');
  }

  saveToken(jwt: any){
    localStorage.setItem('token', jwt.idToken);
  }

  getUser(): Observable<any>{
    return this.http.get(this.API_URL + "/user")
  }

  saveUser(user: any){
    localStorage.setItem('user', user);
  }

  getUserLog(): any {
    let user = localStorage.getItem('user')
    return user != null ? JSON.parse(user) : null;
  }
}
